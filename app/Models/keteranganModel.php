<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\groupModel;
use App\Models\albumModel;

class songModel extends Model
{
    use HasFactory;

    protected $table = 'songs';
    protected $fillable = [
        'title','album_id','group_id'
    ];

    public function band(){
        return $this->belongsTo(groupModel::class, 'group_id', 'id');
    }
    public function album(){
        return $this->belongsTo(albumModel::class, 'album_id', 'id');
    }
}
