<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\albumModel;

class labelModel extends Model
{
    use HasFactory;

    protected $table = 'labels';
    protected $fillable = [
        'name',
    ];

}
