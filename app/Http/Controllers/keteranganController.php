<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\albumModel;
use App\Models\groupModel;
use App\Models\songModel;

class songController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'album' => albumModel::select(['id','name'])->get(),
            'group' => groupModel::select(['id','name'])->get(),
            'data' => songModel::all(),
            'active' => 'song'
        ];
        return view('song',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        songModel::create([
            'title' => $request->name,
            'album_id' => $request->album,
            'group_id' => $request->group,
        ]);
        return redirect('/song')->with('scs','Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'album' => albumModel::select(['id','name'])->get(),
            'group' => groupModel::select(['id','name'])->get(),
            'data' => songModel::find($id),
            'active' => 'song'
        ];
        // dd($data);
        return view('editSong',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        songModel::where('id',$id)
                    ->update([
                        'title' => $request->name,
                        'album_id' => $request->album,
                        'group_id' => $request->group,
                    ]);
        return redirect('/song')->with('scs','Data berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        songModel::find($id)->delete();
        return redirect('/song')->with('scs','Data berhasil dihapus');
    }
}
