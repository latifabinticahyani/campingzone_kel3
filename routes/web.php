<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\groupController;
use App\Http\Controllers\labelController;
use App\Http\Controllers\albumController;
use App\Http\Controllers\songController;
use App\Http\Controllers\userController;
use App\Http\Controllers\baseController;

use App\Http\Middleware\loginCheck;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[baseController::class,'index']);

Route::prefix('auth')->group(function (){
    Route::get('/', [userController::class,'auth']);
    Route::post('/reg', [userController::class,'reg']);
    Route::post('/log', [userController::class,'log']);
    Route::get('/out', [userController::class,'out']);
});

Route::middleware(loginCheck::class)->group(function () {
// Route::middleware()->group(function () {
    
    Route::prefix('user')->group(function (){
        Route::get('/edit', [userController::class,'edit']);
        Route::post('/update', [userController::class,'update']);
    });
    
    Route::get('/home', [userController::class,'home']);
    
    Route::prefix('group')->group(function (){
        Route::get('/', [groupController::class,'index']);
        Route::post('/', [groupController::class,'store']);
        Route::get('/edit/{id}', [groupController::class,'edit']);
        Route::post('/update/{id}', [groupController::class,'update']);
        Route::get('/delete/{id}', [groupController::class,'destroy']);
    });
    
    Route::prefix('label')->group(function (){
        
        Route::get('/', [labelController::class,'index']);
        Route::post('/', [labelController::class,'store']);
        Route::get('/edit/{id}', [labelController::class,'edit']);
        Route::post('/update/{id}', [labelController::class,'update']);
        Route::get('/delete/{id}', [labelController::class,'destroy']);
    });
    
    Route::prefix('album')->group(function (){
        
        Route::get('/', [albumController::class,'index']);
        Route::post('/', [albumController::class,'store']);
        Route::get('/edit/{id}', [albumController::class,'edit']);
        Route::post('/update/{id}', [albumController::class,'update']);
        Route::get('/delete/{id}', [albumController::class,'destroy']);
    });
    
    Route::prefix('song')->group(function (){
        
        Route::get('/', [songController::class,'index']);
        Route::post('/', [songController::class,'store']);
        Route::get('/edit/{id}', [songController::class,'edit']);
        Route::post('/update/{id}', [songController::class,'update']);
        Route::get('/delete/{id}', [songController::class,'destroy']);
    });
});


